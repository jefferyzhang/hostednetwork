// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved

#pragma once

HRESULT
NSModInit
(
);



VOID
NSModDeinit
(
);

class CIcsConnectionInfo : public CRefObject
{
public:
    CIcsConnectionInfo(const CIcsConnection &);

    GUID m_Guid;
    CAtlString m_Name;
    bool m_SharingEnabled;
    bool m_Public;
    bool m_Private;
    bool m_Supported;

    BOOL operator==(GUID& Guid) {return memcmp(&m_Guid, &Guid, sizeof(GUID)) == 0;};
	TCHAR* ToString()
	{
		TCHAR buf[1024]={0};
		
	StringCchPrintf(
		buf,
		1024,
		_T("Guid={%08lX-%04hX-%04hX-%02hhX%02hhX-%02hhX%02hhX%02hhX%02hhX%02hhX%02hhX},Name=%s,share=%s,public=%s,private=%s,supported=%s\n"),
		m_Guid.Data1, m_Guid.Data2, m_Guid.Data3, 
		m_Guid.Data4[0], m_Guid.Data4[1], m_Guid.Data4[2], m_Guid.Data4[3],
		m_Guid.Data4[4], m_Guid.Data4[5], m_Guid.Data4[6], m_Guid.Data4[7],m_Name,
		m_SharingEnabled ? "true" : "false",
	    m_Public ? "true" : "false",
	    m_Private ? "true" : "false",
	    m_Private ? "true" : "false"
		);
		return &buf[0];
	}
};

class CIcsManager
{
    friend class CIcsConnection;

protected:
    INetSharingManager*     m_pNSMgr;
    CIcsConnection*         m_pList;
    bool                    m_bInstalled;
    LONG                    m_lNumConns;
    // cache the indexes of interface on which ICS is enabled
    LONG                    m_lPublicICSIntfIndex;
    LONG                    m_lPrivateICSIntfIndex;

    HRESULT
    GetIndexByGuid
    (
        GUID&       Guid,
        long*       plIndex
    );

public:
    CIcsManager();

    ~CIcsManager();

    HRESULT
    InitIcsManager
    (
    );

    HRESULT
    ResetIcsManager
    (
    );

    HRESULT
    RefreshInstalled
    (
    );

    void
    DisableIcsOnAll
    (
    );

    HRESULT
    EnableIcs
    (
        GUID&   PublicGuid,
        GUID&   PrivateGuid
    );

    void
    GetIcsConnections(CRefObjList<CIcsConnectionInfo *> &);

    // cache the index of interfaces that ICS are enabled
    void
    CacheICSIntfIndex
    (
    );

    // enable the ICS on the cached interfaces
    void
    EnableICSonCache
    (
    );
};