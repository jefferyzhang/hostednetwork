#include "StdAfx.h"
#include "device.h"

_DEVICE_TYPE MatchPredefinedDeviceType(DOT11_MAC_ADDRESS& MacAddress)
{
	//to be added: method of detecting the device type.
	return device_type_default;
}


// CWlanDevice
CWlanDevice::CWlanDevice(
	DOT11_MAC_ADDRESS& MacAddress
	)
{
	// copy MAC address
	memcpy(m_MacAddress, MacAddress, sizeof(DOT11_MAC_ADDRESS));

	// By default, the friendly name is the MAC address
	GetDisplayMacAddress(m_FriendlyName);

	m_Type = MatchPredefinedDeviceType(MacAddress);
}

CWlanDevice::~CWlanDevice()
{
}

BOOL 
	CWlanDevice::operator==(
	const CWlanDevice& Other
	)
{
	// only match the MAC address
	return *this == Other.m_MacAddress;
}

BOOL 
	CWlanDevice::operator==(
	const DOT11_MAC_ADDRESS MacAddress
	)
{
	// only match MAC address
	return memcmp(m_MacAddress, MacAddress, sizeof(DOT11_MAC_ADDRESS)) == 0;
}

VOID 
	CWlanDevice::GetDisplayMacAddress(
	CAtlString& strMacAddress
	)
{
	WCHAR strDisplayName[WLAN_MAX_NAME_LENGTH] = {0}; 
	DWORD szDisplayName = WLAN_MAX_NAME_LENGTH;

	StringCchPrintf(
		strDisplayName,
		szDisplayName,
		L"%02X-%02X-%02X-%02X-%02X-%02X",
		m_MacAddress[0],
		m_MacAddress[1],
		m_MacAddress[2],
		m_MacAddress[3],
		m_MacAddress[4],
		m_MacAddress[5]
	);

	strMacAddress = strDisplayName;
}
