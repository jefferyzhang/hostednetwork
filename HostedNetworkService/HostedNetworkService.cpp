// HostedNetworkService.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_KEY_LEN  63

BOOL gbExit = false;
// ICS manager
CIcsManager * m_IcsMgr;

// Wlan manager
CWlanManager m_WlanMgr;

// Hosted network notification sink
CNotificationSink * m_NotificationSink;

// List of WLAN devices
CRefObjList<CWlanDevice *> m_WlanDeviceList;

// List of connections (adapters)
CRefObjList<CIcsConnectionInfo *> m_ConnectionList;

// whether softAP will be started with full ICS
BOOL m_IcsNeeded;

// ICS enabled
bool m_IcsEnabled;

// Hosted network started
bool m_HostedNetworkStarted;

// GUID of hosted network adapter
GUID m_HostedNetworkGuid;

//// messages for start/stop hosted network
//WCHAR m_strStartHostedNetwork[256];
//WCHAR m_strStopHostedNetwork[256];

// Current name and key for the hosted network
CAtlString m_CurrentName;
CAtlString m_CurrentKey;

// whether the app is running with admin privilege
BOOL m_bIsAdmin;

// whether ICS is allowed 
bool m_bIsICSAllowed;

// private methods
HRESULT InitWlan();
HRESULT InitIcs();
void DeinitIcs();

unsigned  CALLBACK ProcessNotifications(void *);

HRESULT StartHostedNetwork(CString     strName, CString strKey);
void RunCmd(wchar_t* pcmdln);
HRESULT StopHostedNetwork();

void OnDeviceAdd(CWlanDevice *);
void OnDeviceRemove(CWlanDevice *);
void OnDeviceUpdate(CWlanDevice *);
void OnHostedNetworkStarted();
void OnHostedNetworkStopped();
void OnHostedNetworkNotAvailable();
void OnHostedNetworkAvailable();

void GetHostedNetworkInfo();
void GetIcsInfo();

HRESULT StartIcsIfNeeded();
bool StopIcsIfNeeded();
void GetWlanDeviceInfo();

void PostNotification(LPCWSTR msg);
void PostDeviceNotification(CWlanDevice *, int);
void PostErrorMessage(LPCWSTR msg) {_tprintf(msg);OutputDebugString(msg);};

BOOL IsUserAdmin();
bool IsICSAllowed();

bool CheckValidSSIDKeyLen(CString     strName, CString strKey);


void PostDeviceNotification(
	CWlanDevice * pDevice, 
	int Event
	)
{
	_ASSERT(pDevice != NULL);
	bool fPostNotification = true;

	CAtlString strFriendlyName;
	pDevice->GetFriendlyName(strFriendlyName);

	CAtlString msg;

	switch(Event)
	{
	case CHostedNetworkNotification::DeviceAdd:
		msg = L"Device \""+strFriendlyName + L"\" joined the hosted network\n";
		break;

	case CHostedNetworkNotification::DeviceRemove:
		msg = L"Device \""+strFriendlyName + L"\" left the hosted network\n";
		break;

	case CHostedNetworkNotification::DeviceUpdate:
		msg = L"Device \""+strFriendlyName + L"\" was updated\n";
		break;

	default:
		fPostNotification = false;
	}

	if (fPostNotification)
	{
		PostNotification(msg.GetString());    
	}
}

#define MAX_NOTIFICATION_LENGTH 2048

void PostNotification(
	LPCWSTR msg
	) 
{
	// Get current time
	CTime currTime = CTime::GetCurrentTime();
	WCHAR strNotification[MAX_NOTIFICATION_LENGTH + 1] = {0};

	if (SUCCEEDED(StringCchPrintf(
		strNotification,
		MAX_NOTIFICATION_LENGTH,
		L"[%02d/%02d/%04d:%02d:%2d:%02d] %ws",
		currTime.GetMonth(),
		currTime.GetDay(),
		currTime.GetYear(),
		currTime.GetHour(),
		currTime.GetMinute(),
		currTime.GetSecond(),
		msg
		)))
	{
		OutputDebugString(strNotification);
	}
	else
	{
		OutputDebugString(msg);
	}
};



HRESULT InitIcs()
{
	HRESULT hr = S_OK;
	CIcsManager * pIcsMgr = NULL;
	bool fDeinitCom = false;
	bool fDeinitNSMod = false;

	_ASSERT(NULL == m_IcsMgr);

	// initialize NS mod
	hr = NSModInit();
	BAIL_ON_FAILURE(hr);

	fDeinitNSMod = true;

	// initialize COM
	hr = ::CoInitializeEx(
		NULL,
		COINIT_MULTITHREADED
		);
	BAIL_ON_FAILURE(hr);

	fDeinitCom = true;

	// create ICS manager
	pIcsMgr = new(std::nothrow) CIcsManager();
	if (NULL == pIcsMgr)
	{
		hr = E_OUTOFMEMORY;
		BAIL();
	}

	// initialize ICS manager
	hr = pIcsMgr->InitIcsManager();
	BAIL_ON_FAILURE(hr);

	// Everything is fine
	// COM and NSMod are deinitialized later
	fDeinitCom = false;
	fDeinitNSMod = false;

	m_IcsMgr = pIcsMgr;
	pIcsMgr = NULL;

error:
	if (pIcsMgr != NULL)
	{
		delete pIcsMgr;
		pIcsMgr = NULL;
	}

	if (fDeinitCom)
	{
		::CoUninitialize();
	}

	if (fDeinitNSMod)
	{
		NSModDeinit();
	}

	return hr;
}

void DeinitIcs()
{
	if (m_IcsMgr != NULL)
	{
		//
		// ICS was successfully initialized.
		//
		delete m_IcsMgr;
		m_IcsMgr = NULL;

		::CoUninitialize();
		NSModDeinit();
	}
}


BOOL IsUserAdmin()
{
	BOOL                     bIsAdmin              = FALSE;
	SID_IDENTIFIER_AUTHORITY NtAuthority           = SECURITY_NT_AUTHORITY;
	PSID                     AdministratorsGroup; 

	bIsAdmin = AllocateAndInitializeSid(
		&NtAuthority,
		2,
		SECURITY_BUILTIN_DOMAIN_RID,
		DOMAIN_ALIAS_RID_ADMINS,
		0, 
		0, 
		0, 
		0, 
		0, 
		0,
		&AdministratorsGroup
		); 

	if(bIsAdmin) 
	{
		if (!CheckTokenMembership( NULL, AdministratorsGroup, &bIsAdmin)) 
		{
			bIsAdmin = FALSE;
		} 
		FreeSid(AdministratorsGroup); 
	}

	return(bIsAdmin);
}

bool IsICSAllowed()
{
	bool  bIsIcsAllowed = false;
	HKEY  hAllowIcs     = NULL; 
	DWORD dwError       = ERROR_SUCCESS;
	DWORD   ValLen = sizeof(DWORD);
	DWORD   dwAllowIcs = 1;

	dwError = RegOpenKeyEx(
		HKEY_LOCAL_MACHINE,
		L"SOFTWARE\\Policies\\Microsoft\\Windows\\Network Connections",
		NULL,
		KEY_READ,
		&hAllowIcs
		);

	if (ERROR_SUCCESS != dwError || !hAllowIcs)
	{
		BAIL();
	}

	dwError = RegGetValue(
		hAllowIcs,
		NULL,
		L"NC_ShowSharedAccessUI",
		RRF_RT_DWORD,
		NULL,
		&dwAllowIcs,
		&ValLen
		);
	if( dwError == ERROR_SUCCESS && dwAllowIcs == 0)
	{
		bIsIcsAllowed = false;
	}
	else
	{
		bIsIcsAllowed = true;
	}

error:
	if (hAllowIcs)
	{
		RegCloseKey(hAllowIcs);
	}
	return bIsIcsAllowed;
}


HRESULT InitWlan()
{
	HRESULT hr                      = S_OK;
	bool    bIsHostedNetworkStarted = FALSE;

	//
	// Create notification sink
	//
	_ASSERT(m_NotificationSink == NULL);

	m_NotificationSink = new(std::nothrow) CNotificationSink(NULL);
	if (NULL == m_NotificationSink)
	{
		hr = E_OUTOFMEMORY;
		BAIL();
	}

	// 
	// Initialize WLAN manager
	//
	hr = m_WlanMgr.Init();
	BAIL_ON_FAILURE(hr);

	//
	// Set notification sink
	//
	hr = m_WlanMgr.AdviseHostedNetworkNotification(m_NotificationSink); 
	BAIL_ON_FAILURE(hr);

	hr = m_WlanMgr.IsHostedNetworkStarted(bIsHostedNetworkStarted); 
	BAIL_ON_FAILURE(hr);

	if (bIsHostedNetworkStarted)
	{
		OutputDebugString(L"The hosted network is active");
	}

error:
	if (S_OK != hr && m_NotificationSink != NULL)
	{
		delete m_NotificationSink;
		m_NotificationSink = NULL;
	}

	return hr;
}

//
// Get hosted network info and set controls accordingly
//
void GetHostedNetworkInfo()
{
	HRESULT hr = S_OK;
	bool fStarted = false;

	//
	// Get hosted network name
	//
	hr = m_WlanMgr.GetHostedNetworkName(m_CurrentName);
	BAIL_ON_FAILURE(hr);
	PostNotification(m_CurrentName);

	// Get hosted network key
	hr = m_WlanMgr.GetHostedNetworkKey(m_CurrentKey);
	BAIL_ON_FAILURE(hr);
	PostNotification(m_CurrentKey);

	// Get adapter GUID
	m_WlanMgr.GetHostedNetworkInterfaceGuid(m_HostedNetworkGuid);

	//
	// Check whether the hosted network is started or not
	//
	hr = m_WlanMgr.IsHostedNetworkStarted(fStarted);

	_ASSERT(S_OK == hr);
	BAIL_ON_FAILURE(hr);


	// Set the button text
	m_HostedNetworkStarted = fStarted;

	// Disable controls if needed
	if (fStarted)
	{
		PostErrorMessage(L"Hosted network has already started.\n");
	}
	else
	{
		PostErrorMessage(L"Hosted network has not started.starting.\n");
		StartHostedNetwork(m_CurrentName, m_CurrentKey);
	}

error:
	return;
}

void GetWlanDeviceInfo()
{
	CRefObjList<CWlanStation *> stationList;

	if (SUCCEEDED(m_WlanMgr.GetStaionList(stationList)))
	{
		// add stations that are already connected
		while ( 0 != stationList.GetCount() )
		{
			CWlanStation* pStation = stationList.RemoveHead();

			// OnDeviceAdd(pStation);

			pStation->Release();

			pStation = NULL;
		}
	}
}

void RunCmd(wchar_t* pcmdln)
{
	PostNotification(L"RunCmd++");
	wchar_t cmd[ MAX_PATH ];
	size_t nSize = _countof(cmd);
	_wgetenv_s( &nSize, cmd, L"COMSPEC" );

	//wchar_t input[] = L"netsh wlan start hostednetwork";
	// initialize cmd
	wchar_t cmdline[ MAX_PATH + 50 ];
	swprintf_s( cmdline, L"%s /c %s", cmd, pcmdln );
	STARTUPINFOW startInf;
	memset( &startInf, 0, sizeof startInf );
	startInf.cb = sizeof(startInf);
	startInf.wShowWindow = SW_HIDE;
	startInf.dwFlags    |= STARTF_USESHOWWINDOW;
	// If you want to redirect result of command, set startInf.hStdOutput to a file
	// or pipe handle that you can read it, otherwise we are done!
	PROCESS_INFORMATION procInf;
	memset( &procInf, 0, sizeof procInf );
	BOOL b = CreateProcessW( NULL, cmdline, NULL, NULL, FALSE,
		NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW, NULL, NULL, &startInf, &procInf );
	DWORD dwErr = 0;
	if( b ) {
		// Wait till cmd do its job
		WaitForSingleObject( procInf.hProcess, INFINITE );
		// Check whether our command succeeded?
		GetExitCodeProcess( procInf.hProcess, &dwErr );
		// Avoid memory leak by closing process handle
		CloseHandle( procInf.hProcess );
	} else {
		dwErr = GetLastError();
	}
	if( dwErr ) {
		PostErrorMessage(L"Run netsh error\n");
	}
}

//
// Process all notifications in the queue.
// No lock is required because calls are serialized.
//
unsigned  CALLBACK ProcessNotifications(void *pagv)
{
	CHostedNetworkNotification * pNotification = NULL;
	CWlanDevice * pDevice = NULL;
	while(WaitForSingleObject(CNotificationSink::g_NotificatEvent,INFINITE) == WAIT_OBJECT_0)
	{
		if (gbExit)
		{
			break;
		}
		// 
		// Get the first notification
		//
		pNotification = m_NotificationSink->GetNextNotification();

		// pNotification could be NULL

		while (pNotification != NULL)
		{
			pDevice = (CWlanDevice *)pNotification->GetNotificationData();

			switch(pNotification->GetNotificationType())
			{
			case CHostedNetworkNotification::HostedNetworkNotAvailable:
				_ASSERT(NULL == pDevice);
				OnHostedNetworkNotAvailable();
				break;

			case CHostedNetworkNotification::HostedNetworkAvailable:
				_ASSERT(NULL == pDevice);
				OnHostedNetworkAvailable();
				break;

			case CHostedNetworkNotification::HostedNetworkStarted:
				_ASSERT(NULL == pDevice);
				PostNotification(L"Hosted network is now started.\n");
				break;

			case CHostedNetworkNotification::HostedNetworkStopped:
				_ASSERT(NULL == pDevice);
				PostNotification(L"Hosted network is now stopped.\n");
				//if stop then start
				StartHostedNetwork(m_CurrentName, m_CurrentKey);
				break;

			case CHostedNetworkNotification::DeviceAdd:
				_ASSERT(pDevice != NULL);
				if (pDevice != NULL)
				{
					OnDeviceAdd(pDevice);
					PostNotification(L"Device Add .\n");
				}
				break;

			case CHostedNetworkNotification::DeviceRemove:
				_ASSERT(pDevice != NULL);
				if (pDevice != NULL)
				{
					OnDeviceRemove(pDevice);
					PostNotification(L"Device Remove .\n");
				}
				break;

			case CHostedNetworkNotification::DeviceUpdate:
				_ASSERT(pDevice != NULL);
				if (pDevice != NULL)
				{
					OnDeviceUpdate(pDevice);
					PostNotification(L"Device Update .\n");
				}
				break;
			}

			pNotification->Release();
			pNotification = NULL;

			//
			// Get the next notification
			//
			pNotification = m_NotificationSink->GetNextNotification();
		}
	}

	_endthread();
	return 0;
}

bool CheckValidSSIDKeyLen(CString sName, CString sKey)
{
	bool    bSSIDValid   = true;
	bool    bKeyValid    = true;
	bool    bCheckPassed = false;

	if (sName.GetLength() > DOT11_SSID_MAX_LENGTH || sName.GetLength() < 1)
	{
		bSSIDValid = false;
	}

	if (sKey.GetLength() > 63 || sKey.GetLength() < 8)
	{
		bKeyValid = false;
	}

	if (!bSSIDValid && bKeyValid)
	{
		PostErrorMessage(L"Hosted network name should contain 1 ~ 32 case-sensitive characters.\n");
	}
	else if (bSSIDValid && !bKeyValid)
	{
		PostErrorMessage(L"Hosted network key should contain 8 ~ 63 case-sensitive characters.\n");
	}
	else if (!bSSIDValid && !bKeyValid)
	{
		PostErrorMessage(L"Hosted network name should contain 1 ~ 32 case-sensitive characters, and hosted network key contains 8 ~ 63 case-sensitive characters.\n");
	}
	else
	{
		bCheckPassed = true;
	}

	return bCheckPassed;
}


HRESULT StartIcsIfNeeded()
{
	HRESULT hr            = S_OK;
	HRESULT hrPrintError  = S_OK;
	TCHAR   sErrorMsg[256];
	size_t  errorSize     = 256;
	LPCTSTR sErrorFormat  = TEXT("Failed to enable ICS. Error code %x.\n");

	_ASSERT(!m_IcsEnabled);

	if (!m_IcsNeeded)
	{
		// No need to start ICS
		BAIL();
	}

	for (size_t i = 0; i < m_ConnectionList.GetCount(); i++)
	{
		// Get the public connecton
		CIcsConnectionInfo * pConn = m_ConnectionList.GetAt(m_ConnectionList.FindIndex(i));// = (CIcsConnectionInfo *)m_ConnectionComboBox.GetItemDataPtr(m_ConnectionComboBox.GetCurSel());

		if (pConn != NULL)
		{
			PostErrorMessage(pConn->ToString());
			// Start ICS
			hr = m_IcsMgr->EnableIcs(pConn->m_Guid, m_HostedNetworkGuid);

			if (hr != S_OK)
			{
				// reset ICS manager
				hr = m_IcsMgr->ResetIcsManager();

				if (S_OK == hr)
				{
					// try it again
					hr = m_IcsMgr->EnableIcs(pConn->m_Guid, m_HostedNetworkGuid);
				}
			}

			if (S_OK == hr)
			{
				PostNotification(L"Successfully enabled ICS.\n");
				break;
				m_IcsEnabled = true;
			}
			else
			{
				hrPrintError = StringCchPrintf(sErrorMsg, errorSize, sErrorFormat, hr);
				if (hrPrintError != S_OK)
				{
					PostErrorMessage(L"Failed to enable ICS.\n");
					PostNotification(L"Failed to enable ICS.\n");
				}
				else
				{
					PostErrorMessage(sErrorMsg);
					PostNotification(sErrorMsg);
				}
			}
		}

	}

error:
	return hr;
}

bool StopIcsIfNeeded()
{
	bool bICSStopped = false;
	if (m_IcsEnabled && m_IcsNeeded)
	{
		_ASSERT(m_IcsMgr != NULL);

		m_IcsMgr->DisableIcsOnAll();

		PostNotification(L"Successfully disabled ICS.\n");

		m_IcsEnabled = false;
		bICSStopped = true;
	}
	return bICSStopped;
}
void SetEnableIcs(BOOL IcsNeeded)
{
	m_IcsNeeded = IcsNeeded;
}

void ChangeHostednetwork(CString sName, CString sKey)
{
	HRESULT     hr            = S_OK;
	HRESULT     hrPrintError  = S_OK;
	TCHAR       sErrorMsg[256];
	size_t      errorSize     = 256;
	LPCTSTR     sErrorFormat  = TEXT("%s. Error code %x.");
	LPCTSTR     sOperation;
	bool        bValidSSIDKey = false;

	// Check if the lengths of SSID and key are valid
	bValidSSIDKey = CheckValidSSIDKeyLen(sName, sKey);
	if (!bValidSSIDKey)
	{
		return;
	}

	if (m_HostedNetworkStarted)
	{
		hr = StopHostedNetwork();
		sOperation = TEXT("Failed to stop the hostednetwork\n");
		RunCmd(L"netsh wlan stop hostednetwork");
	}
	else
	{
		hr = StartHostedNetwork(sName, sKey);
		sOperation = TEXT("Failed to start the hosted network\n");
		RunCmd(L"netsh wlan start hostednetwork");
	}

	if (FAILED(hr))
	{
		// Post an error message
		hrPrintError = StringCchPrintf(sErrorMsg, errorSize, sErrorFormat, sOperation, hr);
		if (hrPrintError != S_OK)
		{
			PostErrorMessage(sOperation);
		}
		else
		{
			PostErrorMessage(sErrorMsg);
		}
	}
}

void OnHostedNetworkStarted()
{
	// Post a notificatoin
	PostNotification(L"Started using hosted Network.\n");

	// Set state
	m_HostedNetworkStarted = true;

	// Get adapter GUID
	m_WlanMgr.GetHostedNetworkInterfaceGuid(m_HostedNetworkGuid);
}

void OnHostedNetworkStopped()
{
	if (m_HostedNetworkStarted)
	{
		// Post a notificatoin
		PostNotification(L"Stopped using hosted network.\n");

		// Stop ICS if needed
		StopIcsIfNeeded();

		// Set state
		m_HostedNetworkStarted = false;
	}
}

void OnHostedNetworkNotAvailable()
{
	// Post a notificatoin
	PostNotification(L"Hosted network is not available on current NIC(s).\n");
	// Set state
	m_HostedNetworkStarted = false;

	// Stop ICS if needed
	StopIcsIfNeeded();
	ChangeHostednetwork(m_CurrentName, m_CurrentKey);
}

void OnHostedNetworkAvailable()
{
	// Post a notificatoin
	PostNotification(L"Hosted network is available on current NIC(s).\n");

	// Set state
	m_HostedNetworkStarted = false;
	// Get hosted network info
	GetHostedNetworkInfo();

	if(!m_HostedNetworkStarted )
	{
		ChangeHostednetwork(m_CurrentName, m_CurrentKey);
	}

	if (m_bIsAdmin && m_bIsICSAllowed)
	{
		_ASSERT(m_IcsMgr);
		// reset ICS to update ICS list
		m_IcsMgr->ResetIcsManager();

		// ICS list updated
		GetIcsInfo();
	}	
}

void OnDeviceAdd(
	CWlanDevice * pDevice
	)
{
	_ASSERT(pDevice != NULL);

	if (pDevice != NULL)
	{
		// The device should not be in the list
		_ASSERT(!m_WlanDeviceList.IsInArray(pDevice));

		pDevice->AddRef();

		// Add the device to the list
		m_WlanDeviceList.AddTail(pDevice);

		// Post a notification
		PostDeviceNotification(pDevice, CHostedNetworkNotification::DeviceAdd);
	}
}

void OnDeviceRemove(
	CWlanDevice * pDevice
	)
{
	_ASSERT(pDevice != NULL);
	CWlanDevice * pRemovedDevice = NULL;

	//
	// Find the device and remove it from the device list
	//
	for (size_t i = 0; i < m_WlanDeviceList.GetCount(); i++)
	{
		POSITION pos = m_WlanDeviceList.FindIndex(i);
		CWlanDevice * pTmpDevice = m_WlanDeviceList.GetAt(pos);
		if (*pTmpDevice == *pDevice)
		{
			//
			// Found the device, remove it from the list
			//
			m_WlanDeviceList.RemoveAt(pos);
			pRemovedDevice = pTmpDevice;
			break;
		}
	}

	if (pRemovedDevice != NULL)
	{
		// Post a notification
		PostDeviceNotification(pDevice, CHostedNetworkNotification::DeviceRemove);

		pRemovedDevice->Release();
		pRemovedDevice = NULL;
	}
}

void OnDeviceUpdate(
	CWlanDevice * pDevice
	)
{
	_ASSERT(pDevice != NULL);
	CWlanDevice * pRemovedDevice = NULL;
	POSITION pos;

	//
	// Find the device and remove it from the device list
	//
	for (size_t i = 0; i < m_WlanDeviceList.GetCount(); i++)
	{
		pos = m_WlanDeviceList.FindIndex(i);
		CWlanDevice * pTmpDevice = m_WlanDeviceList.GetAt(pos);
		if (*pTmpDevice == *pDevice)
		{
			//
			// Found the device, update it with the new object
			//
			pDevice->AddRef();
			m_WlanDeviceList.SetAt(pos, pDevice);
			pRemovedDevice = pTmpDevice;
			break;
		}
	}

	if (pRemovedDevice != NULL)
	{
		// Post a notification
		PostDeviceNotification(pDevice, CHostedNetworkNotification::DeviceUpdate);

		pRemovedDevice->Release();
		pRemovedDevice = NULL;
	}
}

HRESULT StartHostedNetwork(CString     strName, CString strKey)
{
	HRESULT     hr                    = S_OK;
	CAtlString  name;
	CAtlString  key;
	bool        bHostedNetworkStarted = false;
	bool        bICSStarted           = false;

	name = strName; 
	if (m_CurrentName != name)
	{
		// Set name only when needed
		hr = m_WlanMgr.SetHostedNetworkName(name);
		BAIL_ON_FAILURE(hr);
		m_CurrentName = name;
	}

	key = strKey;
	if (m_CurrentKey != key)
	{
		// Set key only when needed
		hr = m_WlanMgr.SetHostedNetworkKey(key);
		BAIL_ON_FAILURE(hr);
		m_CurrentKey = key;
	}

	// Cache the current ICS-enabled interfaces
	if (m_IcsNeeded)
	{
		m_IcsMgr->CacheICSIntfIndex();

		// Start ICS if:
		// 1. User has selected that softAP starts with full ICS, and
		// 2. the current ICS setting is different from the new setting
		hr = StartIcsIfNeeded();
		bICSStarted = true;

		// Force stop the currently running hosted network,
		// if there is any
		// this step is taken no matter  whether the start ICS 
		// succeeds or fails
		m_WlanMgr.IsHostedNetworkStarted(bHostedNetworkStarted);
		if (bHostedNetworkStarted)
		{
			m_WlanMgr.ForceStopHostedNetwork();
		}

		// if start ICS fails, bail out
		BAIL_ON_FAILURE(hr);
	}

	// Start hosted network
	hr = m_WlanMgr.StartHostedNetwork();

	// if start hosted network fails, bail out
	BAIL_ON_FAILURE(hr);

	m_WlanMgr.IsHostedNetworkStarted(bHostedNetworkStarted);
	OnHostedNetworkStarted();

error:
	if (hr != S_OK)
	{
		RunCmd(L"netsh wlan start hostednetwork");
		if (m_IcsNeeded && bICSStarted)
		{
			// restore the previous ICS settings
			m_IcsMgr->EnableICSonCache();
		}
	}
	return hr;
}

HRESULT StopHostedNetwork()
{
	HRESULT hr = S_OK;
	bool bIcsStopped = false;

	// Stop full ICS if ICS is needed
	bIcsStopped = StopIcsIfNeeded();

	// If a previous running ICS is stopped
	// force stop the hosted network
	// otherwise, stop using the hosted network
	if (bIcsStopped)
	{
		hr = m_WlanMgr.ForceStopHostedNetwork();
	}
	else
	{
		hr = m_WlanMgr.StopHostedNetwork();
	}

	BAIL_ON_FAILURE(hr);

	OnHostedNetworkStopped();
error:
	return hr;
}

void GetIcsInfo()
{
	// Get the list of ICS connections
	m_IcsMgr->GetIcsConnections(m_ConnectionList);

	// If the hosted network is started, check whether ICS is enabled for hosted network
	if (m_HostedNetworkStarted)
	{
		for (size_t i = 0; i < m_ConnectionList.GetCount(); i++)
		{
			CIcsConnectionInfo * pConn = m_ConnectionList.GetAt(m_ConnectionList.FindIndex(i));
			if (*pConn == m_HostedNetworkGuid)
			{
				m_IcsEnabled = (pConn->m_Supported && pConn->m_SharingEnabled && pConn->m_Private);
				break;
			}
		}
	}

	// Update control
	if (m_IcsEnabled)
	{
		m_IcsNeeded = true;
	}

}
typedef struct __tagCommandLineParam
{
	BOOL debug;
	BOOL hidewin;
	BOOL IcsNeeded;
	BOOL exitsrv;
	__tagCommandLineParam()
	{
		debug = FALSE;
		hidewin = false;
		IcsNeeded = FALSE;
		exitsrv = false;
	}

}COMMANDLINEPARAM, *PCOMMANDLINEPARAM;

COMMANDLINEPARAM g_args;

void read_command_line(int argc, _TCHAR* argv[])
{
	for (int i=0; i<argc; i++)
	{
		if (argv[i]!=NULL && (argv[i][0]==L'-' || argv[i][0]==L'/'))
		{
			TCHAR* v=_tcschr(argv[i], L'=');
			if(v!=NULL) v++;
			if (_tcsnicmp(&argv[i][1], _T("debug"), 5)==0)
			{
				g_args.debug=TRUE;
			}
			else if(_tcsnicmp(&argv[i][1], _T("hide"), _tcslen(_T("hide")))==0)
			{
				g_args.hidewin = TRUE;
			}
			else if(_tcsnicmp(&argv[i][1], _T("icsneeded"), _tcslen(_T("icsneeded")))==0)
			{
				g_args.IcsNeeded = TRUE;
			}
			else if(_tcsnicmp(&argv[i][1], _T("exitsrv"), _tcslen(_T("exitsrv")))==0)
			{
				g_args.exitsrv = TRUE;
			}
		}
	}
}


int _tmain(int argc, _TCHAR* argv[])
{
	read_command_line(argc, argv);
	if(g_args.hidewin)
	{
		HWND myConsole = GetConsoleWindow();
		ShowWindow(myConsole,0);
	}
	if(g_args.exitsrv)
	{
		HANDLE hEventExit =OpenEvent(EVENT_ALL_ACCESS|SYNCHRONIZE,FALSE,_T("Global\\HostedNetworkWatchDog"));
		if(hEventExit)
		{
			SetEvent(hEventExit);
		}
		return 0;
	}

	m_NotificationSink = (NULL);
	m_IcsNeeded=(false);
	m_IcsEnabled =(false);
	m_HostedNetworkStarted=(false);
	m_IcsMgr=NULL;

	SetEnableIcs(g_args.IcsNeeded);

	//// Load the messages for start/stop hosted network
	//LoadString (
	//	GetModuleHandle(NULL),
	//	IDS_START_HOSTED_NETWORK,
	//	m_strStartHostedNetwork,
	//	256
	//	);

	//LoadString (
	//	GetModuleHandle(NULL),
	//	IDS_STOP_HOSTED_NETWORK,
	//	m_strStopHostedNetwork,
	//	256
	//	);

	m_bIsAdmin = IsUserAdmin();
	m_bIsICSAllowed = IsICSAllowed();

	if (!m_bIsAdmin || !m_bIsICSAllowed)
	{    
		// Disable ICS controls
		m_IcsNeeded = FALSE;

		if (!m_bIsAdmin)
		{
			PostErrorMessage(L"The application is not running with Admin privilege, user will not have access to ICS controls.\n");
		}
		else
		{
			PostErrorMessage(L"ICS is disabled by domain administrator, user will not have access to ICS controls.\n");
		}
	}
	// Step 2: if user has admin privilege and ICS is allowed in the domain,
	// initialize ICS manager
	if (m_bIsAdmin && m_bIsICSAllowed)
	{
		// Initialize ICS
		if (FAILED(InitIcs()))
		{
			// Post a notificatoin
			PostNotification(L"Failed to initialize ICS manager.\n");

			BAIL();
		}

		// Post a notificatoin
		PostNotification(L"Successfully initialized ICS manager.\n");

		// Get connection info
		m_IcsMgr->GetIcsConnections(m_ConnectionList);
	}

	// Following code is to Initialize WLAN
	if (FAILED(InitWlan()))
	{
		// Post a notificatoin
		PostErrorMessage(L"Failed to initialize WiFi manager.\n");
		BAIL();
	}

	// Post a notificatoin
	PostNotification(L"Successfully initialized WiFi manager.\n");

	// Get hosted network info
	GetHostedNetworkInfo();

	// Get ICS info. Must be called after GetHostedNetworkInfo().
	if (m_bIsAdmin && m_bIsICSAllowed)
	{
		GetIcsInfo();
	}

	// Set state
	m_HostedNetworkStarted = false;


	unsigned int threadid=0;
	HANDLE hEventExit = CreateEvent(NULL, FALSE, FALSE, _T("Global\\HostedNetworkWatchDog"));
	_beginthreadex(0,0,ProcessNotifications, NULL, 0, &threadid);
	if(WaitForSingleObject(hEventExit, INFINITE)==WAIT_OBJECT_0)
	{

	}
	gbExit = TRUE;
	SetEvent(CNotificationSink::g_NotificatEvent);

error:
	return -1;


	if (m_NotificationSink != NULL)
	{
		m_WlanMgr.UnadviseHostedNetworkNotification();

		delete m_NotificationSink;

		m_NotificationSink = NULL;
	}

	DeinitIcs();

	_ASSERT(m_IcsMgr == NULL);


	return 0;
}

