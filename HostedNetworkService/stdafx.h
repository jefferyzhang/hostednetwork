// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <stdlib.h>
#include <wchar.h>
#include <strsafe.h>
#include <netcon.h>
#include <new>

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

#include <atlbase.h>
#include <atlstr.h>
#include <atltime.h>

// TODO: reference additional headers your program requires here
#include <atlcoll.h>

#include <wlanapi.h>

#include "common.h"
#include "wlanmgr.h"
#include "device.h"
#include "notif.h"
#include "icsconn.h"
#include "icsmgr.h"