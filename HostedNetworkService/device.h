#pragma once
#ifndef _DEVICE_H_
#define _DEVICE_H_

typedef enum _DEVICE_TYPE
{
	device_type_default = 0,
	device_type_camera,
	device_type_printer,
	device_type_computer,
	device_type_telephone,
	device_type_zune,
	device_type_invalid
} _DEVICE_TYPE;

class CWlanDevice : public CRefObject
{
private:
	DOT11_MAC_ADDRESS m_MacAddress;

	// Don't allow to create an empty CWlanDevice object
	CWlanDevice() {};

	// Friendly hame
	CAtlString m_FriendlyName;

	enum _DEVICE_TYPE m_Type;
public:
	CWlanDevice(DOT11_MAC_ADDRESS&);
	~CWlanDevice();

	BOOL operator==(const CWlanDevice &);
	BOOL operator==(const DOT11_MAC_ADDRESS);

	VOID GetFriendlyName(CAtlString& strName) {strName = m_FriendlyName;};

	VOID SetFriendlyName(__in LPWSTR strName) {m_FriendlyName = strName;};

	VOID SetType(_DEVICE_TYPE Type) {m_Type = Type;};

	VOID GetDisplayMacAddress(CAtlString&);

	VOID GetMacAddress(DOT11_MAC_ADDRESS & MacAddress) {memcpy(MacAddress, m_MacAddress, sizeof(DOT11_MAC_ADDRESS));};
};
#endif
